#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import telegram
from time import sleep
from os import getenv
import requests
from pdf2image import convert_from_bytes
import io
import re
import shelve

TOKEN = getenv('TOKEN', '123123123123123')
WHITELIST = ['martiri', 'cammeo', 'betti']
MAIN_URL = 'https://www.dsu.toscana.it/servizi/ristorazione/dove-e-cosa-mangiare/i-menu/'

storage = shelve.open('storage')


def already_sent(url):
    try:
        return storage[url]
    except KeyError:
        return False

def get_pdf_urls():
    res = requests.get(MAIN_URL)
    urls = re.findall(
        'http://www.dsu.toscana.it/it/.*\.pdf',
        res.content.decode('UTF-8')
    )
    return urls


def convert(url):
    res = requests.get(url)
    images = convert_from_bytes(res.content)
    imgByteArr = io.BytesIO()
    imgByteArr.name = 'image.jpeg'
    images[0].save(imgByteArr, 'JPEG')
    imgByteArr.seek(0)
    return imgByteArr


def send(bot, url, keyword=None):
    caption = '#' + keyword if keyword else ''
    bot.send_photo('@mensaUnipi', photo=convert(url), caption=caption)
    storage[url] = True
    storage.sync()


def main():
    bot = telegram.Bot(TOKEN)
    for url in get_pdf_urls():
        for name in WHITELIST:
            if not already_sent(url) and name in url.lower():
                try:
                    send(bot, url, keyword=name)
                except Exception as e:
                    print('Error while converting ' + url)
                    print(e)


if __name__ == '__main__':
    main()
